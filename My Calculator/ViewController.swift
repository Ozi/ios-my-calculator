//
//  ViewController.swift
//  My Calculator
//
//  Created by Muchamad Fauzi on 02/03/23.
//

import UIKit

class ViewController: UIViewController {
    
    private lazy var holder: UIView = {
        let h = UIView()
        h.backgroundColor = .black
        h.translatesAutoresizingMaskIntoConstraints = false
        return h
    }()
    
    var firstNumber = 0
    var resultNumber = 0
    var currentOperations: Operation?
    var workings: String = ""
    
    enum Operation {
        case add, subtract, multiply, devide
    }
    
    private lazy var workingsLabel: UILabel = {
        let work = UILabel()
        work.text = ""
        work.textColor = .white
        work.textAlignment = .right
        work.numberOfLines = 2
        work.font = UIFont(name: "Helvetica", size: 60)
        
        return work
    }()
    
    private lazy var resultLabel: UILabel = {
        let label = UILabel()
        label.text = "0"
        label.textColor = .white
        label.textAlignment = .right
        label.font = UIFont(name: "Helvetica", size: 90)
        
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(holder)
        holder.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        holder.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        holder.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        holder.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupNumberPad()
    }
    
    private func createUIButton(titleText: String = "",xtag: Int = 0, x: Double = 0, y: Double = 0, width: Double = 0, height: Double = 0, tag: Int = 0, background: UIColor = .white, titleColor: UIColor = .black) -> UIButton {
        
        let button = UIButton(frame: CGRect(x: x, y: y, width: width, height: height))
        button.setTitleColor(titleColor, for: .normal)
        button.backgroundColor = background
        button.setTitle(titleText, for: .normal)
        button.tag = Int(xtag) + tag
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 0.5).cgColor
        return button
    }
    
    private func setupNumberPad() {
        let buttonSize = view.frame.size.width / 4

        let zeroButton = createUIButton(titleText: "0", x: buttonSize*1, y: holder.frame.size.height-buttonSize, width: buttonSize, height: buttonSize, tag: 1)
        zeroButton.addTarget(self, action: #selector(zeroTapped), for: .touchUpInside)
        holder.addSubview(zeroButton)
        
        let decimalButton = createUIButton(titleText: ".", y: holder.frame.size.height-buttonSize, width: buttonSize, height: buttonSize, tag: 1)
        decimalButton.addTarget(self, action: #selector(decimalTap), for: .touchUpInside)
        holder.addSubview(decimalButton)
        
        for x in 0..<3 {
            let button1 = createUIButton(titleText: "\(x+1)", xtag: x, x: buttonSize * CGFloat(x), y: holder.frame.size.height-(buttonSize*2), width: buttonSize, height: buttonSize, tag: 2)
            button1.addTarget(self, action: #selector(numberPressed), for: .touchUpInside)
            holder.addSubview(button1)
        }
        
        for x in 0..<3 {
            let button2 = createUIButton(titleText: "\(x+4)", xtag: x, x: buttonSize * CGFloat(x), y: holder.frame.size.height-(buttonSize*3), width: buttonSize, height: buttonSize, tag: 5)
            button2.addTarget(self, action: #selector(numberPressed(_:)), for: .touchUpInside)
            holder.addSubview(button2)
        }
        
        for x in 0..<3 {
            let button3 = createUIButton(titleText: "\(x+7)", xtag: x, x: buttonSize * CGFloat(x), y: holder.frame.size.height-(buttonSize*4), width: buttonSize, height: buttonSize, tag: 8)
            button3.addTarget(self, action: #selector(numberPressed(_:)), for: .touchUpInside)
            holder.addSubview(button3)
        }
        
        let clearButton =  createUIButton(titleText: "AC", y: holder.frame.size.height-(buttonSize*5), width: buttonSize, height: buttonSize, titleColor: .red)
        clearButton.addTarget(self, action: #selector(clearResult), for: .touchUpInside)
        holder.addSubview(clearButton)

        let deleteButton = createUIButton(titleText: "Del", x: buttonSize*1, y: holder.frame.size.height-(buttonSize*5), width: buttonSize, height: buttonSize, titleColor: .red)
        deleteButton.addTarget(self, action: #selector(backTap), for: .touchUpInside)
        holder.addSubview(deleteButton)
        
        let percentButton = createUIButton(titleText: "%", x: buttonSize*2, y: holder.frame.size.height-(buttonSize*5), width: buttonSize, height: buttonSize, background: .orange)
        percentButton.addTarget(self, action: #selector(percentTap), for: .touchUpInside)
        holder.addSubview(percentButton)
        
        let equals = createUIButton(titleText: "=", x: buttonSize*2, y: holder.frame.size.height-(buttonSize*1), width: buttonSize*2, height: buttonSize, background: .orange)
        equals.addTarget(self, action: #selector(equalsTap), for: .touchUpInside)
        holder.addSubview(equals)

        let addButton = createUIButton(titleText: "+", x: buttonSize*3, y: holder.frame.size.height-(buttonSize*2), width: buttonSize, height: buttonSize, background: .orange)
        addButton.addTarget(self, action: #selector(addTap), for: .touchUpInside)
        holder.addSubview(addButton)
        
        let subtractButton = createUIButton(titleText: "-", x: buttonSize*3, y: holder.frame.size.height-(buttonSize*3), width: buttonSize, height: buttonSize, background: .orange)
        subtractButton.addTarget(self, action: #selector(subtractTap), for: .touchUpInside)
        holder.addSubview(subtractButton)
        
        let multiplyButton = createUIButton(titleText: "*", x: buttonSize*3, y: holder.frame.size.height-(buttonSize*4), width: buttonSize, height: buttonSize, background: .orange)
        multiplyButton.addTarget(self, action: #selector(multiplyTap), for: .touchUpInside)
        holder.addSubview(multiplyButton)
        
        let devideButton = createUIButton(titleText: "/", x: buttonSize*3, y: holder.frame.size.height-(buttonSize*5), width: buttonSize, height: buttonSize, background: .orange)
        devideButton.addTarget(self, action: #selector(devideTap), for: .touchUpInside)
        holder.addSubview(devideButton)
        
        workingsLabel.frame = CGRect(x: 20, y: holder.frame.origin.y - 20, width: view.frame.size.width - 40, height: 150)
        holder.addSubview(workingsLabel)
        
        resultLabel.frame = CGRect(x: 20, y: clearButton.frame.origin.y - 110, width: view.frame.size.width - 40, height: 100)
        holder.addSubview(resultLabel)
    }
    
    @objc func addToWorkings(value: String) {
        workings = workings + value
        workingsLabel.text = workings
    }
    
    @objc func backTap() {
        if(!workings.isEmpty) {
            workings.removeLast()
            workingsLabel.text = workings
        }
    }
    
    @objc func clearResult() {
        resultLabel.text = "0"
        workings = ""
        workingsLabel.text = ""
        firstNumber = 0
    }
    
    @objc func zeroTapped() {
        if workingsLabel.text != "" {
            addToWorkings(value: "0")
        }
    }
    
    @objc func numberPressed(_ sender: Any) {
        let tag = (sender as AnyObject).tag - 1
        addToWorkings(value: "\(tag)")
    }
    
    @objc func equalsTap(_ sender: Any) {
        
        if(validInput()) {
            let checkedWorkingsForPercent = workings.replacingOccurrences(of: "%", with: "*0.01")
            let expression = NSExpression(format: checkedWorkingsForPercent)
            let result = expression.expressionValue(with: nil, context: nil) as! Double
            let resultString = formatResult(result: result)
            resultLabel.text = resultString
        }else {
            let alert = UIAlertController(
            title: "Invalid Input",
            message: "Calculator unable to do math on input based ",
            preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "Okey", style: .default))
            self.present(alert, animated: true, completion: nil)
        } 
    }
    
    func validInput() -> Bool {
        var count = 0
        var funcCharIndexes = [Int]()
        
        for char in workings {
            if(specialCharacter(char: char)) {
                funcCharIndexes.append(count)
            }
            count += 1
        }
        
        var previous: Int = -1
        
        for index in funcCharIndexes {
            if(index == 0) {
                return false
            }
            
            if(index == workings.count - 1) {
                return false
            }
            
            if (previous != -1) {
                if(index - previous == 1) {
                    return false
                }
            }
            previous = index
        }
        return true
    }
    
    func specialCharacter(char: Character) -> Bool {
        if(char == "*") {
            return true
        }
        if(char == "/") {
            return true
        }
        if(char == "+") {
            return true
        }
        if(char == "%") {
            return true
        }
        if(char == "-") {
            return true
        }
        if(char == "=") {
            return true
        }
        return false
    }
    
    func formatResult(result: Double) -> String {
        if(result.truncatingRemainder(dividingBy: 1) == 0) {
            return String(format: "%.0f", result)
        }else {
            return String(format: "%.2f", result)
        }
    }
    
    @objc func decimalTap(_ sender: Any) {
        addToWorkings(value: ".")
    }
    
    @objc func percentTap(_ sender: Any) {
        addToWorkings(value: "%")
    }
    
    @objc func addTap(_ sender: Any) {
        addToWorkings(value: "+")
    }
    
    @objc func subtractTap(_ sender: Any) {
        addToWorkings(value: "-")
    }
    
    @objc func multiplyTap(_ sender: Any) {
        addToWorkings(value: "*")
    }
    
    @objc func devideTap(_ sender: Any) {
        addToWorkings(value: "/")
    }

}

